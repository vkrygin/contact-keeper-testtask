module.exports = {
    preset: "jest-playwright-preset",
    testMatch: ["**/__tests__/**/*.+(ts|js)", "**/?(*.)+(spec|test).+(ts|js)"],
    testEnvironmentOptions: {
        "jest-playwright": {
            browsers: ["chromium"],
        }
    },
    reporters: [
        "default",
        ["./node_modules/jest-html-reporter", {
            "pageTitle": "Test Report",
            "includeFailureMsg": true
        }]
    ]
}