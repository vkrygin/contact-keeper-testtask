# Contact-keeper-testTask
**Запуск тестов**
1. Склонировать репозиторий
1. npm i
1. npm test

После прогона тестов результаты тестирования можно посмотреть в файле test-report.html в корне репозитория

**Запуск тестов в CI**
1. Перейти на страницу https://gitlab.com/vkrygin/contact-keeper-testtask/-/pipelines/new (потребуется авторизоваться и запросить доступ к проекту)
1. Run pipeline
1. Результаты выполнения всех пайплайнов можно посмотреть вот тут https://gitlab.com/vkrygin/contact-keeper-testtask/-/pipelines. Тут можно и без доступа


Запустится джоб, после выполнения которого можно выгрузить файл с артефактами test-report.html, который будет содержать результаты выполнения тестов.
Или же можно browse artifacts -> test-report.html для просмотра результатов тестирования без загрузки архива.
```
```
Завел документ в гугл доках, куда выписал все найденные баги
https://docs.google.com/document/d/1BYxGCxlTTWTmFiLS9l8JOYGqNMyknqe-XPAgwaNf990/edit?usp=sharing
