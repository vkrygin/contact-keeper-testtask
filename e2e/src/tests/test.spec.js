const { webkit, chromium, firefox } = require('playwright')
const { loginPage } = require('../pages/loginPage')
const { contactPage } = require('../pages/contactPage')
const { uiAssertions } = require('../helpers/uiAssertions')
const { apiAssertions } = require('../helpers/apiAssertions')
const { waiters } = require('../helpers/waiters')
const { contactsPageLocators } = require('../elements/contactsPage')
const { elementStates } = require('../helpers/elementStates')
const { testData } = require('../testData/testData')
const { urlList } = require('../testData/urlList')

describe('Contacts page', () => {
    const logIn = new loginPage(page)
    const contacts = new contactPage(page)
    const ui = new uiAssertions()
    const api = new apiAssertions()
    const wait = new waiters()

    beforeAll(async () => {
        await logIn.authorize(testData.userData.userEmail, testData.userData.pass)
    })

    it('Should create empty contact', async () => {
        await ui.innerTextEquals(contactsPageLocators.editorHeader, testData.pageHeaders.onCreateHeader)

        await contacts.clickSaveBtn()

        await api.assertRespCode(urlList.apiURLs.contactsAssertionsRoute, 422)
    })

    it('Should fill only contact\'s name', async () => {
        await ui.innerTextEquals(contactsPageLocators.editorHeader, testData.pageHeaders.onCreateHeader)

        await contacts.fillContactCard(testData.contactAttrs.onlyName, testData.emptyString, testData.emptyString)
        await contacts.saveContact()

        await ui.innerTextContains(contactsPageLocators.contactName, testData.contactAttrs.onlyName)
    })

    it('Should fill only contact\'s email', async () => {
        await ui.innerTextEquals(contactsPageLocators.editorHeader, testData.pageHeaders.onCreateHeader)

        await contacts.fillContactCard(testData.emptyString, testData.contactAttrs.email, testData.emptyString)
        await contacts.clickSaveBtn()

        await api.assertRespCode(urlList.apiURLs.contactsAssertionsRoute, 422)
    })

    it('Should fill only contact\'s phone', async () => {
        await ui.innerTextEquals(contactsPageLocators.editorHeader, testData.pageHeaders.onCreateHeader)

        await contacts.fillContactCard(testData.emptyString, testData.emptyString, testData.contactAttrs.phone)
        await contacts.clickSaveBtn()

        await api.assertRespCode(urlList.apiURLs.contactsAssertionsRoute, 422)
    })

    it('Should create contact without name', async () => {
        await ui.innerTextEquals(contactsPageLocators.editorHeader, testData.pageHeaders.onCreateHeader)

        await contacts.fillContactCard(testData.emptyString, testData.contactAttrs.email, testData.contactAttrs.phone)
        await contacts.clickSaveBtn()

        await api.assertRespCode(urlList.apiURLs.contactsAssertionsRoute, 422)
    })

    it('Should create contact without email', async () => {
        await ui.innerTextEquals(contactsPageLocators.editorHeader, testData.pageHeaders.onCreateHeader)

        await contacts.fillContactCard(testData.contactAttrs.withoutEmail, testData.emptyString, testData.contactAttrs.phoneWithoutEmail)
        await contacts.saveContact()

        await ui.innerTextContains(contactsPageLocators.contactName, testData.contactAttrs.withoutEmail)
        await ui.innerTextEquals(contactsPageLocators.contactEmail, testData.contactAttrs.phoneWithoutEmail)
    })

    it('Should create contact without phone', async () => {
        await ui.innerTextEquals(contactsPageLocators.editorHeader, testData.pageHeaders.onCreateHeader)

        await contacts.fillContactCard(testData.contactAttrs.withoutPhone, testData.contactAttrs.emailWithoutPhone, testData.emptyString)
        await contacts.saveContact()

        await ui.innerTextContains(contactsPageLocators.contactName, testData.contactAttrs.withoutPhone)
        await ui.innerTextEquals(contactsPageLocators.contactEmail, testData.contactAttrs.emailWithoutPhone)
    })

    it('Should create contact with invalid email', async () => {
        await ui.innerTextEquals(contactsPageLocators.editorHeader, testData.pageHeaders.onCreateHeader)

        await contacts.fillContactCard(testData.contactAttrs.name, testData.contactAttrs.invalidEmail, testData.contactAttrs.phone)
        await contacts.clickSaveBtn()

        await ui.innerTextEquals(contactsPageLocators.editorHeader, testData.pageHeaders.onCreateHeader)
        await ui.inputValueEquals(contactsPageLocators.contactField('Имя'), testData.contactAttrs.name)
        await ui.inputValueEquals(contactsPageLocators.contactField('Email'), testData.contactAttrs.invalidEmail)
        await ui.inputValueEquals(contactsPageLocators.contactField('Телефон'), testData.contactAttrs.phone)
    })

    it('Should create contact with RF domain email (fails)', async () => {
        await ui.innerTextEquals(contactsPageLocators.editorHeader, testData.pageHeaders.onCreateHeader)

        await contacts.fillContactCard(testData.contactAttrs.name, testData.contactAttrs.rfDomainEmail, testData.contactAttrs.phone)
        await contacts.saveContact()

        await contacts.assertContact(testData.contactAttrs.name, testData.contactAttrs.rfDomainEmail, testData.contactAttrs.phone, testData.contactTypes.personal)
    })

    it('Should create contact with invalid phone (fails)', async () => {
        await ui.innerTextEquals(contactsPageLocators.editorHeader, testData.pageHeaders.onCreateHeader)

        await contacts.fillContactCard(testData.contactAttrs.name, testData.contactAttrs.email, testData.contactAttrs.invalidPhone)
        await contacts.clickSaveBtn()

        await api.assertRespCode(urlList.apiURLs.contactsAssertionsRoute, 422)
    })

    it('Should create contact with long name', async () => {
        await ui.innerTextEquals(contactsPageLocators.editorHeader, testData.pageHeaders.onCreateHeader)

        await contacts.fillContactCard(testData.contactAttrs.longNameOrPhone, testData.emptyString, testData.emptyString)
        await contacts.clickSaveBtn()

        await api.assertRespCode(urlList.apiURLs.contactsAssertionsRoute, 422)
    })

    it('Should create contact with long email (fails)', async () => {
        await ui.innerTextEquals(contactsPageLocators.editorHeader, testData.pageHeaders.onCreateHeader)

        await contacts.fillContactCard(testData.contactAttrs.name, testData.contactAttrs.longEmail, testData.emptyString)
        await contacts.clickSaveBtn()

        await api.assertRespCode(urlList.apiURLs.contactsAssertionsRoute, 422)
    })

    it('Should create contact with long phone number (fails)', async () => {
        await ui.innerTextEquals(contactsPageLocators.editorHeader, testData.pageHeaders.onCreateHeader)

        await contacts.fillContactCard(testData.contactAttrs.name, testData.emptyString, testData.contactAttrs.longNameOrPhone)
        await contacts.clickSaveBtn()

        await api.assertRespCode(urlList.apiURLs.contactsAssertionsRoute, 422)
    })

    it('Should create contact with all attributes filled', async () => {
        await ui.innerTextEquals(contactsPageLocators.editorHeader, testData.pageHeaders.onCreateHeader)

        await contacts.fillContactCard(testData.contactAttrs.name, testData.contactAttrs.email, testData.contactAttrs.phone)
        await contacts.saveContact()

        await contacts.assertContact(testData.contactAttrs.name, testData.contactAttrs.email, testData.contactAttrs.phone, testData.contactTypes.personal)
    })

    it('Should edit contacts\' name', async () => {
        await contacts.clickEditBtn()
        await contacts.fillName(testData.contactAttrs.editedName)
        await contacts.saveContact()

        await contacts.assertContact(testData.contactAttrs.editedName, testData.contactAttrs.email, testData.contactAttrs.phone, testData.contactTypes.personal)
    })

    it('Should edit contacts\' email', async () => {
        await contacts.clickEditBtn()
        await contacts.fillEmail(testData.contactAttrs.editedEmail)
        await contacts.saveContact()

        await contacts.assertContact(testData.contactAttrs.editedName, testData.contactAttrs.editedEmail, testData.contactAttrs.phone, testData.contactTypes.personal)
    })

    it('Should edit contacts\' phone', async () => {
        await contacts.clickEditBtn()
        await contacts.fillPhone(testData.contactAttrs.editedPhone)
        await contacts.saveContact()

        await contacts.assertContact(testData.contactAttrs.editedName, testData.contactAttrs.editedEmail, testData.contactAttrs.editedPhone, testData.contactTypes.personal)
    })

    it('Should edit contact type', async () => {
        await contacts.clickEditBtn()
        await contacts.selectType(testData.contactTypes.professional)
        await contacts.saveContact()

        await contacts.assertContact(testData.contactAttrs.editedName, testData.contactAttrs.editedEmail, testData.contactAttrs.editedPhone, testData.contactTypes.professional)
    })

    it('Should clear all contact\' fields', async () => {
        await contacts.clickEditBtn()
        await contacts.fillContactCard(testData.emptyString, testData.emptyString, testData.contactAttrs.phone)
        await contacts.clickSaveBtn()

        const response = await page.waitForResponse((resp) =>
            resp.url().includes(urlList.apiURLs.contactsRoute))
        expect(response.status()).toBe(422)
    })

    it('Should edit all contact fields', async () => {
        await contacts.clickEditBtn()
        await contacts.fillContactCard(testData.contactAttrs.name, testData.contactAttrs.email, testData.contactAttrs.phone)
        await contacts.selectType(testData.contactTypes.personal)
        await contacts.saveContact()

        await contacts.assertContact(testData.contactAttrs.name, testData.contactAttrs.email, testData.contactAttrs.phone, testData.contactTypes.personal)
    })

    it('Should edit contact with invalid email', async () => {
        await contacts.clickEditBtn()
        await contacts.fillEmail(testData.contactAttrs.invalidEmail)
        await contacts.clickSaveBtn()

        await ui.innerTextEquals(contactsPageLocators.editorHeader, testData.pageHeaders.onEditHeader)
        await ui.inputValueEquals(contactsPageLocators.contactField('Имя'), testData.contactAttrs.name)
        await ui.inputValueEquals(contactsPageLocators.contactField('Email'), testData.contactAttrs.invalidEmail)
        await ui.inputValueEquals(contactsPageLocators.contactField('Телефон'), testData.contactAttrs.phone)
    })

    it('Should press cancel button when editing', async () => {
        await contacts.clickEditBtn()
        await contacts.clickCancelBtn()

        await ui.innerTextEquals(contactsPageLocators.editorHeader, testData.pageHeaders.onCreateHeader)
        await ui.inputValueEquals(contactsPageLocators.contactField('Имя'), testData.emptyString)
        await ui.inputValueEquals(contactsPageLocators.contactField('Email'), testData.emptyString)
        await ui.inputValueEquals(contactsPageLocators.contactField('Телефон'), testData.emptyString)
    })

    it('Should delete contact', async () => {
        contacts.deleteContact()
    })

    it('Should delete all contacts', async () => {
        await page.$$eval(contactsPageLocators.deleteBtn, btns => btns.forEach(btn => btn.click()))

        await wait.waitForElState(contactsPageLocators.contactCard, elementStates.detached)
        expect(await (await page.$$(contactsPageLocators.deleteBtn)).length).toBe(0)
    })

    afterAll(async () => {
        await browser.close()
    })
})