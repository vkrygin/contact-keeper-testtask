const { loginPageLocators } = require('../elements/loginPage')
const { waiters } = require('../helpers/waiters')
const { elementStates } = require('../helpers/elementStates')
const { urlList } = require('../testData/urlList')

class loginPage {
    wait = new waiters()

    constructor(page) { this.page = page }

    async fillUsername(username) { await this.page.fill(loginPageLocators.userEmail, username) }

    async fillPassword(pass) { await this.page.fill(loginPageLocators.userPass, pass) }

    async clickSignInBtn() { this.page.click(loginPageLocators.signInBtn) }

    async authorize(username, pass) {
        await this.page.goto(urlList.uiURLs.loginPage)
        await this.fillUsername(username)
        await this.fillPassword(pass)
        await this.clickSignInBtn()
        await this.wait.waitForRedirect(urlList.uiURLs.homePage)
        await this.wait.waitForElState(loginPageLocators.spinner, elementStates.detached)
    }
}

module.exports = { loginPage }