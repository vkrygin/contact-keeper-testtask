const { contactsPageLocators } = require('../elements/contactsPage')
const { uiAssertions } = require('../helpers/uiAssertions')
const { waiters } = require('../helpers/waiters')
const { elementStates } = require('../helpers/elementStates')
const { testData } = require('../testData/testData')
const { urlList } = require('../testData/urlList')

class contactPage {
    ui = new uiAssertions()
    wait = new waiters()

    constructor(page) { this.page = page }

    async fillName(name) { await this.page.fill(contactsPageLocators.contactField('Имя'), name) }

    async fillEmail(email) { await this.page.fill(contactsPageLocators.contactField('Email'), email) }

    async fillPhone(phone) { await this.page.fill(contactsPageLocators.contactField('Телефон'), phone) }

    async clickSaveBtn() { await this.page.click(contactsPageLocators.saveBtn) }

    async clickCancelBtn() { await this.page.click(contactsPageLocators.cancelBtn) }

    async selectType(type) {
        if (type == testData.contactTypes.professional) {
            await this.page.click(contactsPageLocators.radioButton('professional'))
        }
        else if (type == testData.contactTypes.personal) {
            await this.page.click(contactsPageLocators.radioButton('personal'))
        }
        else {
            throw new Error('Wrong contact type value. Should be "work" or "personal"')
        }
    }

    async fillContactCard(name, email, phone) {
        await this.fillName(name)
        await this.fillEmail(email)
        await this.fillPhone(phone)
    }

    async saveContact() {
        await this.clickSaveBtn()
        await Promise.all([
            this.page.waitForResponse((resp) =>
                resp.url().includes(urlList.apiURLs.contactsRoute)),
            this.page.waitForSelector(contactsPageLocators.contactCard),
            this.wait.waitForElState(contactsPageLocators.cancelBtn, elementStates.detached)
        ])
    }

    async assertContact(name, email, phone, type) {
        await this.ui.innerTextContains(contactsPageLocators.contactName, name)
        await this.ui.innerTextEquals(contactsPageLocators.contactEmail, email)
        await this.ui.innerTextEquals(contactsPageLocators.contactPhone, phone)
        await this.ui.innerTextEquals(contactsPageLocators.contactType, type)
    }

    async clickEditBtn() {
        await this.page.click(contactsPageLocators.editBtn)
        await this.ui.innerTextEquals(contactsPageLocators.editorHeader, testData.pageHeaders.onEditHeader)
    }

    async deleteContact() {
        await this.wait.waitForElState(contactsPageLocators.deleteBtn, elementStates.visible)
        await this.wait.waitForElState(contactsPageLocators.contactCard, elementStates.visible)
        await this.page.click(contactsPageLocators.deleteBtn)
        await this.wait.waitForElState(contactsPageLocators.contactCard, elementStates.detached)
    }
}

module.exports = { contactPage }