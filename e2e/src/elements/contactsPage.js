let contactsPageLocators = {
    editorHeader: ':nth-child(1) > form > .text-primary',

    contactField: (el) => `[placeholder="${el}"]`,
    radioButton: (el) => `[value="${el}"]`,
    saveBtn: '[type="submit"]',
    cancelBtn: '.btn-light',

    contactCard: '.card',
    contactName: '.card > .text-primary',
    contactEmail: '.list > :nth-child(1)',
    contactPhone: '.list > :nth-child(2)',
    contactType: '.badge',
    editBtn: '.btn-dark',
    deleteBtn: '.btn-danger'
}

module.exports = { contactsPageLocators }