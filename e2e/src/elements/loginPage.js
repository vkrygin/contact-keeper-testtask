let loginPageLocators = {
    userEmail : 'input[name="email"]',
    userPass : 'input[name="password"]',
    signInBtn : 'input:has-text("Войти")',
    spinner : '[class^="loadingio-spinner"]'
}

module.exports = { loginPageLocators }