let testData = {
    emptyString: '',

    userData: {
        userEmail: 'sample@mail.com',
        pass: '123456'
    },

    contactTypes: {
        personal: 'Личный',
        professional: 'Рабочий'
    },

    pageHeaders: {
        onCreateHeader: 'Добавить контакт',
        onEditHeader: 'Редактировать контакт'
    },

    contactAttrs: {
        name: 'Алексей Григорьевич Игнатьев',
        email: 'alexey.ignatiev@email.com',
        phone: '88005553535',

        editedName: 'Сергей Семенович Алексейченко',
        editedEmail: 'alexeychenko.sergey@email.com',
        editedPhone: '+7-800-555-35-35',

        rfDomainEmail: 'москва@почта.рф',

        invalidEmail: 'abc123',
        invalidPhone: 'abc',

        longNameOrPhone: '1234567890'.repeat(7),
        longEmail: `${'1234567890'.repeat(7)}@mail.com`,

        onlyName: 'Петров Иван Петрович',
        withoutEmail: 'Иван Петрович Иванов',
        phoneWithoutEmail: '+7-861-523-12-44',

        withoutPhone: 'Смирнова Наталья Ивановна',
        emailWithoutPhone: 'smirnova@email.com',
    }
}

module.exports = { testData }