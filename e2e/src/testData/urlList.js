let urlList = {
    uiURLs: {
        loginPage: 'https://kontact-book.herokuapp.com/login',
        homePage: 'https://kontact-book.herokuapp.com/'
    },

    apiURLs: {
        contactsRoute: '/api/contacts',
        contactsAssertionsRoute: '**/api/contacts'
    }
}

module.exports = { urlList }