class uiAssertions {
    async innerTextContains(el, text) {
        expect(await page.$eval(el, e => e.innerText)).toEqual(
            expect.stringContaining(text)
        )
    }

    async innerTextEquals(el, text) {
        expect(await page.$eval(el, e => e.innerText)).toBe(text)
    }

    async inputValueEquals(el, value) {
        expect(await page.$eval(el, e => e.value)).toBe(value)
    }
}

module.exports = { uiAssertions }