class apiAssertions {
    async assertRespCode(url, code) {
        const response = await page.waitForResponse(url)
        expect(response.status()).toBe(code)
    }
}

module.exports = { apiAssertions }