let elementStates = {
    detached: 'detached',
    visible: 'visible'
}

module.exports = { elementStates }