class waiters {
    async waitForElState(selector, elState) {
        await page.waitForSelector(selector, { state: elState })
    }

    async waitForRedirect(urlString) {
        await page.waitForNavigation({ url: urlString })
    }
}

module.exports = { waiters }